<?php
/**
 * Created by PhpStorm.
 * User: c5101
 * Date: 2017/5/23
 * Time: 上午 03:03
 */

namespace Tpk\Means;


class UnitConversion
{
    public static function ToBytes ($from)
    {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number * 1024;
            case "MB":
                return $number * pow(1024, 2);
            case "GB":
                return $number * pow(1024, 3);
            case "TB":
                return $number * pow(1024, 4);
            case "PB":
                return $number * pow(1024, 5);
            default:
                return $from;
        }
    }

    public static function BytesTo ($bytes, $unit = false)
    {
        $types = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $bytes >= 1024 && $i < (count($types) - 1); $bytes /= 1024, $i++) ;

        $unit = (!$unit) ? null: $types[$i];

        return (round($bytes, 2) . " " . $unit);
    }
}