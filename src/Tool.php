<?php
/**
 * Created by PhpStorm.
 * User: c5101
 * Date: 2017/5/21
 * Time: 下午 06:08
 */

namespace Tpk\Means;

class Tool
{
    public static function Output ($check, $Description = null)
    {
        echo '<div class="col-md-4"><pre>';
        echo "<h5>{$Description}</h5>";

        if (is_array($check) || is_object($check)) {
            var_export($check);
        } else {
            echo $check;
        }

        echo "</pre></div>";

    }

    public static function Conversion ($value,$unit = false)
    {
        $show = (!is_numeric($value)) ? "ToBytes" : "BytesTo";

        return UnitConversion::$show($value, $unit);
    }
}